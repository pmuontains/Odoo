@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

if exist C:\Bitnami\ODOO-9~1.201\mysql\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mysql\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\postgresql\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\postgresql\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\logstash\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\apache2\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache2\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\resin\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\resin\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\jboss\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\jboss\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\wildfly\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\wildfly\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\activemq\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\activemq\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\openoffice\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\openoffice\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\subversion\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\subversion\scripts\serviceinstall.bat INSTALL)
rem RUBY_APPLICATION_INSTALL
if exist C:\Bitnami\ODOO-9~1.201\mongodb\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mongodb\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\lucene\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\lucene\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\third_application\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\third_application\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\nginx\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\nginx\scripts\serviceinstall.bat INSTALL)
if exist C:\Bitnami\ODOO-9~1.201\php\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\php\scripts\serviceinstall.bat INSTALL)
goto end

:remove

if exist C:\Bitnami\ODOO-9~1.201\third_application\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\third_application\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\lucene\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\lucene\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\mongodb\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mongodb\scripts\serviceinstall.bat)
rem RUBY_APPLICATION_REMOVE
if exist C:\Bitnami\ODOO-9~1.201\subversion\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\subversion\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\openoffice\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\openoffice\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\jboss\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\jboss\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\wildfly\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\wildfly\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\resin\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\resin\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\activemq\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\activemq\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\apache2\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache2\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\logstash\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\postgresql\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\postgresql\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\mysql\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mysql\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\php\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\php\scripts\serviceinstall.bat)
if exist C:\Bitnami\ODOO-9~1.201\nginx\scripts\serviceinstall.bat (start /MIN C:\Bitnami\ODOO-9~1.201\nginx\scripts\serviceinstall.bat)
:end
