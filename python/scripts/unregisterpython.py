import sys
from _winreg import *

version = sys.version[:3]

regpath = "SOFTWARE\\Python\\Pythoncore\\%s\\" % version
installkey = "InstallPath"
pythonkey = "PythonPath"

def UnRegisterPy():
    try:
        reg = OpenKey(HKEY_LOCAL_MACHINE, regpath)
    except EnvironmentError:
        print "*** Python not registered?!"
        return
    try:
        DeleteKey(reg, installkey)
        DeleteKey(reg, pythonkey)
        DeleteKey(HKEY_LOCAL_MACHINE, regpath)
    except:
        print "*** Unable to un-register!"
    else:
        print "--- Python", version, "is no longer registered!"
if __name__ == "__main__":
    UnRegisterPy()
