@echo off
CALL "C:\Bitnami\odoo-9.0.20160620-0/scripts/setenv.bat"

rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

python "C:\Bitnami\odoo-9.0.20160620-0/python/scripts/registerpython.py"
goto end

:remove
python "C:\Bitnami\odoo-9.0.20160620-0/python/scripts/unregisterpython.py"
goto end

:end
exit
