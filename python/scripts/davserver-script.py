#!C:\Bitnami\ODOO-9~1.201\python\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'pywebdav==0.9.4.1','console_scripts','davserver'
__requires__ = 'pywebdav==0.9.4.1'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('pywebdav==0.9.4.1', 'console_scripts', 'davserver')()
    )
