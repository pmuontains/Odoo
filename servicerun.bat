@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop

if exist C:\Bitnami\ODOO-9~1.201\hypersonic\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\server\hsql-sample-database\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\ingres\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\ingres\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\mysql\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mysql\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\postgresql\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\postgresql\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\logstash\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\apache2\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache2\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\openoffice\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\openoffice\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\resin\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\resin\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\activemq\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\activemq\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\jboss\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\jboss\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\wildfly\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\wildfly\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\jetty\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\jetty\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\subversion\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\subversion\scripts\servicerun.bat START)
rem RUBY_APPLICATION_START
if exist C:\Bitnami\ODOO-9~1.201\lucene\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\lucene\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\mongodb\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mongodb\scripts\servicerun.bat START)
if exist C:\Bitnami\ODOO-9~1.201\third_application\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\third_application\scripts\servicerun.bat START)
goto end

:stop
echo "Stopping services ..."
if exist C:\Bitnami\ODOO-9~1.201\third_application\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\third_application\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\lucene\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\lucene\scripts\servicerun.bat STOP)
rem RUBY_APPLICATION_STOP
if exist C:\Bitnami\ODOO-9~1.201\subversion\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\subversion\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\jetty\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\jetty\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\hypersonic\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\server\hsql-sample-database\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\jboss\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\jboss\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\wildfly\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\wildfly\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\resin\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\resin\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\activemq\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\activemq\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\servicerun.bat (start /MIN /WAIT C:\Bitnami\ODOO-9~1.201\apache-tomcat\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\openoffice\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\openoffice\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\apache2\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\apache2\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash-forwarder\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\logstash\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\logstash\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\elasticsearch\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\ingres\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\ingres\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\mysql\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mysql\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\mongodb\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\mongodb\scripts\servicerun.bat STOP)
if exist C:\Bitnami\ODOO-9~1.201\postgresql\scripts\servicerun.bat (start /MIN C:\Bitnami\ODOO-9~1.201\postgresql\scripts\servicerun.bat STOP)

:end
