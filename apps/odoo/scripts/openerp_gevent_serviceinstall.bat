@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:/Bitnami/odoo-9.0.20160620-0/apps/odoo\scripts\openerp_gevent.exe" install
net start odooGevent-1 > NUL

goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop odooGevent-1 > NUL

"C:/Bitnami/odoo-9.0.20160620-0/apps/odoo\scripts\openerp_gevent.exe" uninstall

:end
exit
