@echo off

rem START or STOP Openerp-Gevent Service
rem --------------------------------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop
C:/Bitnami/odoo-9.0.20160620-0/python\python.exe "C:/Bitnami/odoo-9.0.20160620-0/apps/odoo\scripts\openerp-gevent" -c "C:/Bitnami/odoo-9.0.20160620-0/apps/odoo\conf\openerp-server.conf"
goto end

:stop
set /p openerpPID=<C:/Bitnami/odoo-9.0.20160620-0/apps/odoo/openerp-gevent.pid
taskkill /PID %openerpPID% /F
del C:/Bitnami/odoo-9.0.20160620-0/apps/odoo/openerp-gevent.pid

:end
exit