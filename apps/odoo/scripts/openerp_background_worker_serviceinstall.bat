@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:/Bitnami/odoo-9.0.20160620-0/apps/odoo\scripts\openerp_background_worker.exe" install
net start odooBackgroundWorker-1 > NUL

goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop odooBackgroundWorker-1 > NUL

"C:/Bitnami/odoo-9.0.20160620-0/apps/odoo\scripts\openerp_background_worker.exe" uninstall

:end
exit
