@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:\Bitnami\odoo-9.0.20160620-0/postgresql\bin\pg_ctl.exe" register -N "odooPostgreSQL-1" -D "C:\Bitnami\odoo-9.0.20160620-0/postgresql/data"

net start "odooPostgreSQL-1" >NUL
goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop "odooPostgreSQL-1" >NUL
"C:\Bitnami\odoo-9.0.20160620-0/postgresql\bin\pg_ctl.exe" unregister -N "odooPostgreSQL-1"


:end
exit
