@echo off
rem START or STOP Apache Service
rem --------------------------------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop

net start odooApache-1
goto end

:stop

"C:/Bitnami/odoo-9.0.20160620-0/apache2\bin\httpd.exe" -n "odooApache-1" -k stop

:end
exit
