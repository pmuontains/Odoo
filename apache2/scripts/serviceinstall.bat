@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:/Bitnami/odoo-9.0.20160620-0/apache2\bin\httpd.exe" -k install -n "odooApache-1" -f "C:/Bitnami/odoo-9.0.20160620-0/apache2\conf\httpd.conf"

net start odooApache-1 >NUL
goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop odooApache-1 >NUL
"C:/Bitnami/odoo-9.0.20160620-0/apache2\bin\httpd.exe" -k uninstall -n "odooApache-1"

:end
exit
